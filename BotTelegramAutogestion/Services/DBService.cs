﻿using System;
using System.Collections.Generic;
using System.Linq;
using BotTelegramAutogestion.DataLayer;

namespace BotTelegramAutogestion.Services
{
    public class DBHelper
    {
        public static void NewLogin(long user, string legajo, string pwd, string esp, string chatUsername)
        {
            using (var db = new DBManager())
            {
                var userDB = db.Users.Find(user);
                if (userDB == null)
                {
                    var nuevoLogin = new User
                    {
                        Activo = true,
                        Especialidad = esp,
                        FechaUltimaRequest = DateTime.UtcNow.Subtract(TimeSpan.FromHours(3)),
                        FechaUltimoMensaje = DateTimeOffset.FromUnixTimeMilliseconds(0).DateTime,
                        Legajo = legajo,
                        Password = EncryptionHelper.Encrypt256(pwd),
                        UserId = user,
                        TelegramAlias = chatUsername
                    };
                    db.Users.Add(nuevoLogin);
                }
                else
                {
                    //ya está en la DB
                    if (!userDB.Activo)
                        throw new Exception("El usuario se encuentra inactivo/baneado para el uso del Bot");
                    userDB.Especialidad = esp;
                    userDB.Legajo = legajo;
                    userDB.Password = EncryptionHelper.Encrypt256(pwd);
                    userDB.FechaUltimaRequest = DateTime.UtcNow;
                    userDB.FechaUltimoMensaje = DateTimeOffset.FromUnixTimeMilliseconds(0).DateTime;
                }

                db.SaveChanges();
            }
        }

        public static User GetCredentialsForUser(long telegramUser)
        {
            using (var db = new DBManager())
            {
                var userDB = db.Users.Find(telegramUser);

                if (userDB == null)
                    throw new Exception("El Usuario no se encuentra en la Base de datos, porfavor, realice un /login");

                if (!userDB.Activo)
                    throw new Exception(
                        $"El usuario {userDB.Legajo}@{userDB.Especialidad} se encuentra inhabilitado para el uso del bot");

                var newUserReference = new User
                {
                    Activo = userDB.Activo,
                    Especialidad = userDB.Especialidad,
                    Legajo = userDB.Legajo,
                    Password = EncryptionHelper.Decrypt256(userDB.Password),
                    AutomaticoActive = userDB.AutomaticoActive,
                    AutomaticoPermitido = userDB.AutomaticoPermitido,
                    TelegramAlias = userDB.TelegramAlias,
                    TiempoActualizacion = userDB.TiempoActualizacion,
                    UserId = userDB.UserId,
                    FechaUltimaRequest = userDB.FechaUltimaRequest,
                    FechaUltimoMensaje = userDB.FechaUltimoMensaje
                };


                return newUserReference;
            }
        }

        public static void ActualizarFechaUltimoMensaje(long userId, long fechaPublicacionCompleta)
        {
            using (var db = new DBManager())
            {
                var userDB = db.Users.Find(userId);
                if (userDB == null) throw new Exception("❗No se encontró el usuario en la DB - Contactar Devs");

                userDB.FechaUltimoMensaje = DateTimeOffset.FromUnixTimeMilliseconds(fechaPublicacionCompleta).DateTime;
                db.SaveChanges();
            }
        }

        public static long ObtenerUltimaFechaMensajes(long userId)
        {
            using (var db = new DBManager())
            {
                var userDB = db.Users.Find(userId);
                if (userDB == null) throw new Exception("❗No se encontró el usuario en la DB - Contactar Devs");
                var t = (long) userDB.FechaUltimoMensaje.ToUniversalTime()
                    .Subtract(new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)).TotalMilliseconds;
                return t;
                db.Dispose();
            }
        }

        public static bool IsAuthorizedAutomaticRetrieve(long chatId)
        {
            using (var db = new DBManager())
            {
                var user = db.Users.Find(chatId);
                if (user == null) return false;

                return user.Activo && user.AutomaticoPermitido;
            }
        }

        public static int GetAutomatedMinutes(long chatId)
        {
            using (var db = new DBManager())
            {
                var user = db.Users.Find(chatId);
                if (user == null) return 0;

                return user.TiempoActualizacion.Value;
            }
        }

        public static bool ActiveUser(long chatId)
        {
            using (var db = new DBManager())
            {
                var user = db.Users.Find(chatId);
                if (user == null) return false;

                return user.Activo;
            }
        }

        public static bool AutomaticIsActive(long chatId)
        {
            using (var db = new DBManager())
            {
                var user = db.Users.Find(chatId);
                if (user == null) return false;

                return user.AutomaticoActive;
            }
        }

        public static int CanAutomatize(long chatId)
        {
            using (var db = new DBManager())
            {
                var user = db.Users.Find(chatId);
                if (user == null) return -1;

                if (user.AutomaticoActive && user.AutomaticoPermitido && user.Activo)
                    return user.TiempoActualizacion.Value;
                return -1;
            }
        }

        public static void SetAutomaticState(long chatId, bool daemonDaemonActive)
        {
            using (var db = new DBManager())
            {
                var user = db.Users.Find(chatId);
                if (user == null) return;

                user.AutomaticoActive = daemonDaemonActive;
                db.SaveChanges();
            }
        }

        public static List<long> RetrieveAllAutomaticActives()
        {
            var chats = new List<long>();

            using (var db = new DBManager())
            {
                //if (db.Users.Any()) return chats;
                var usuarios = db.Users.Where(u => u.Activo && u.AutomaticoActive && u.AutomaticoPermitido);

                foreach (var user in usuarios) chats.Add(user.UserId);
                db.Dispose();
            }

            return chats;
        }

        public static void UpdateLastRequestDate(long chatId)
        {
            using (var db = new DBManager())
            {
                var user = db.Users.Find(chatId);
                if (user == null) return;

                user.FechaUltimaRequest = DateTime.UtcNow;
                db.SaveChanges();
            }
        }

        public static DateTime GetLastRequestDate(long chatId)
        {
            var lastDate = DateTime.MinValue;
            using (var db = new DBManager())
            {
                var user = db.Users.Find(chatId);
                if (user == null) return DateTime.MinValue;

                lastDate = user.FechaUltimaRequest;

                db.Dispose();
            }

            return lastDate;
        }

        public static List<long> GetAllChatsID()
        {
            List<long> chatsIds = new List<long>();

            using (DBManager db = new DBManager())
            {
                var dbusers = db.Users;
                foreach (var user in dbusers)
                {
                    chatsIds.Add(user.UserId);
                }

                return chatsIds;
            }
            
            
        }
    }
}