﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using BotTelegramAutogestion.Logic;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace BotTelegramAutogestion.Services
{
    public static class BotSettings
    {
        private static IConfigurationRoot config;

        private static Dictionary<string, string> configuration;

        public static string BotToken
        {
            get
            {
                var enVar = Environment.GetEnvironmentVariable("BOT_TOKEN");

                if (string.IsNullOrEmpty(enVar)) return configuration["BOT_TOKEN"];

                return enVar;
            }
        }

        public static string AesIV256
        {
            get
            {
                var enVar = Environment.GetEnvironmentVariable("AES_IV_256");

                if (string.IsNullOrEmpty(enVar)) return configuration["AES_IV_256"];

                return enVar;
            }
        }

        public static string AesKey256
        {
            get
            {
                var enVar = Environment.GetEnvironmentVariable("AES_KEY_256");

                if (string.IsNullOrEmpty(enVar)) return configuration["AES_KEY_256"];

                return enVar;
            }
        }

        public static string DBConnectionString
        {
            get
            {
                var host = Environment.GetEnvironmentVariable("HOST_DB");
                var nameDB = Environment.GetEnvironmentVariable("NAME_DB");
                var user = Environment.GetEnvironmentVariable("USER_DB");
                var pwd = Environment.GetEnvironmentVariable("PWD_DB");


                if (string.IsNullOrEmpty(host) || string.IsNullOrEmpty(nameDB) || string.IsNullOrEmpty(user) ||
                    string.IsNullOrEmpty(pwd))
                {
                    host = config["HOST_DB"];
                    nameDB = config["NAME_DB"];
                    user = config["USER_DB"];
                    pwd = config["PWD_DB"];
                }

                var output = $"Host={host};Database={nameDB};Username={user};Password={pwd};SSL Mode=Require;TrustServerCertificate=True";
                
                //Console.WriteLine($"[DB] {output}");
                
                return output;
            }
        }

        public static string Version
        {
            get
            {
                string version = Environment.GetEnvironmentVariable("VERSION");

                if (string.IsNullOrEmpty(version))
                {
                    version = config["VERSION"];
                }

                return version;
            }
        }

        public static bool LoadDaemons
        {
            get
            {
                var enVar = Environment.GetEnvironmentVariable("LOAD_DAEMONS");

                if (string.IsNullOrEmpty(enVar)) return configuration["LOAD_DAEMONS"].Equals("1");

                 return enVar.Equals("1");
            }
        }

        public static bool Receive { get
        {
            var enVar = Environment.GetEnvironmentVariable("RECEIVE");

            if (string.IsNullOrEmpty(enVar)) return configuration["RECEIVE"].Equals("1");

            return enVar.Equals("1");
        } }

        public static bool SendBootMessage { get
        {
            var enVar = Environment.GetEnvironmentVariable("SEND_BOOTMESSAGE");

            if (string.IsNullOrEmpty(enVar)) return configuration["SEND_BOOTMESSAGE"].Equals("1");

            return enVar.Equals("1");
        } }

        public static string BootMessage { get
        {
            var enVar = Environment.GetEnvironmentVariable("BOOTMESSAGE");

            if (string.IsNullOrEmpty(enVar)) return configuration["BOOTMESSAGE"];

            return enVar;
        } }

        public static void ConfigureServices(IServiceCollection serviceCollection)
        {
            // Build configuration
            config = new ConfigurationBuilder()
                .SetBasePath(Directory.GetParent(AppContext.BaseDirectory).FullName)
                .AddJsonFile("appsettings.json", false)
                .Build();

            
            // Add access to generic IConfigurationRoot
            serviceCollection.AddSingleton(config);

            // Add app
            serviceCollection.AddTransient<Program>();

            configuration = config.GetChildren().ToDictionary(x => x.Key, x => x.Value);
        }
    }
}