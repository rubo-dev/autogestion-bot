﻿using System;
using BotTelegramAutogestion.Services;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace BotTelegramAutogestion.DataLayer
{
    public partial class DBManager : DbContext
    {
        public DBManager()
        {
        }

        public DBManager(DbContextOptions<DBManager> options)
            : base(options)
        {
        }

        public virtual DbSet<User> Users { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseNpgsql(BotSettings.DBConnectionString);
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("Relational:Collation", "en_US.UTF-8");

            modelBuilder.Entity<User>(entity =>
            {
                entity.ToTable("USERS", "AUTOGESTION_BOT_DATA");

                entity.Property(e => e.UserId)
                    .ValueGeneratedNever()
                    .HasColumnName("userId");

                entity.Property(e => e.Activo).HasColumnName("activo");

                entity.Property(e => e.AutomaticoActive).HasColumnName("automaticoActive");

                entity.Property(e => e.AutomaticoPermitido).HasColumnName("automaticoPermitido");

                entity.Property(e => e.Especialidad)
                    .IsRequired()
                    .HasColumnName("especialidad");

                entity.Property(e => e.FechaUltimaRequest)
                    .HasColumnType("timestamp with time zone")
                    .HasColumnName("fechaUltimaRequest");

                entity.Property(e => e.FechaUltimoMensaje)
                    .HasColumnType("timestamp with time zone")
                    .HasColumnName("fechaUltimoMensaje");

                entity.Property(e => e.Legajo)
                    .IsRequired()
                    .HasColumnName("legajo");

                entity.Property(e => e.Password)
                    .IsRequired()
                    .HasColumnName("password");

                entity.Property(e => e.TelegramAlias).HasColumnName("telegramAlias");

                entity.Property(e => e.TiempoActualizacion)
                    .HasColumnName("tiempoActualizacion")
                    .HasDefaultValueSql("60");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}