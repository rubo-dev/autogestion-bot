﻿using System;

#nullable disable

namespace BotTelegramAutogestion.DataLayer
{
    public class User
    {
        public string Especialidad { get; set; }
        public DateTime FechaUltimaRequest { get; set; }
        public DateTime FechaUltimoMensaje { get; set; }
        public string Legajo { get; set; }
        public string Password { get; set; }
        public long UserId { get; set; }
        public bool Activo { get; set; }
        public int? TiempoActualizacion { get; set; }
        public bool AutomaticoActive { get; set; }
        public string TelegramAlias { get; set; }
        public bool AutomaticoPermitido { get; set; }
    }
}