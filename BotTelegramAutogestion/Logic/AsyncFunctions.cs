﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using BotTelegramAutogestion.Models;
using BotTelegramAutogestion.Services;
using Telegram.Bot.Args;
using Telegram.Bot.Types.Enums;
using Telegram.Bot.Types.ReplyMarkups;

namespace BotTelegramAutogestion.Logic
{
    public static class AsyncFunctions
    { 
      
        public static async void GenerarMensajeLogin(MessageEventArgs eventArgs)
        {
            try
            {
                var mensaje = eventArgs.Message.Text;

                if (string.IsNullOrEmpty(mensaje)) return;

                // /login 74533 contra esp
                if (mensaje.Split(" ").Length != 4)
                {
                    Program.botClient.SendTextMessageAsync(eventArgs.Message.Chat.Id,
                        "Revisá la sintaxis del comando, enviá /login para mas info, o es posible que tu contrañena tenga espacios");
                    return;
                }

                //if (botClient.GetChatAsync(eventArgs.Message.Chat.Id).GetAwaiter().GetResult().PinnedMessage != null) botClient.UnpinChatMessageAsync(eventArgs.Message.Chat.Id);
                var cred = mensaje.Substring(7); //quita el /login·

                var user = cred.Split(" ")[0];
                var pwd = cred.Split(" ")[1];
                var esp = cred.Split(" ")[2];

                try
                {
                    DBHelper.NewLogin(eventArgs.Message.Chat.Id, user, pwd, esp, eventArgs.Message.Chat.Username);
                }
                catch (Exception e)
                {
                    Program.botClient.SendTextMessageAsync(eventArgs.Message.Chat.Id, e.Message);
                    return;
                }

                var messageID = Program.botClient.SendTextMessageAsync(eventArgs.Message.Chat.Id,
                    "El login en el bot fue exitoso✅,\npresiona el boton de abajo para recibir actualizaciones de mensajes.Tambien podes usar el comando /automatico para que el bot solo por vos busque los mensajes (funcion limitada)\n\nTené en cuenta que el bot " +
                    "aun está en fase de pruebas y con un hosteo de bajo presupuesto.\n" +
                    $"Cualquier sugerencia es bienvenida.\nLos devs estamos en la descripción\n\n\nbotversion:v{BotSettings.Version}",
                    ParseMode.Default,
                    replyMarkup: new InlineKeyboardMarkup(new[]
                    {
                        new InlineKeyboardButton
                        {
                            Text = "Recuperar nuevos mensajes",
                            CallbackData = "retrieve"
                        }
                    })).Result.MessageId;
            }
            catch (Exception e)
            {
                Program.botClient.SendTextMessageAsync(eventArgs.Message.Chat.Id,
                    "‼Ocurrió un problema mientras procesabamos tu solicitud. \n\n❌Puede ser porque autogestión no esté disponible en este momento. \n\n" +
                    "Recordá que si cambiaste la contraseña el bot no tiene forma de averiguarlo. Si es así, logueate de nuevo con /login !\n" +
                    "Gracias por usar el bot, y lamentamos el error 🤗", disableNotification: true);
                Console.WriteLine("[UNHANDLED-EXCEPTION] " + e);
            }
        }
        
        public static async void EnviarMensajesAutogestion(long chatId, List<ModelMessage> mensajes, bool daemonCheck=false)
        {
            try
            {
                int messageIdInicio = 0;
                
                if (!daemonCheck)
                {
                    var messageIdInicioTask = Program.botClient.SendTextMessageAsync(chatId, "🔄 Buscando nuevos mensajes...", disableNotification: true);
                    messageIdInicioTask.Wait();
                    messageIdInicio = messageIdInicioTask.Result.MessageId;
                }
                
                Program.botClient.SendChatActionAsync(chatId, ChatAction.Typing);
                
                var fechaUltimoMensajeUsuario = DBHelper.ObtenerUltimaFechaMensajes(chatId);

                mensajes = mensajes.RemoveOldMessages(fechaUltimoMensajeUsuario);

                
                Program.botClient.SendChatActionAsync(chatId, ChatAction.Typing);

                foreach (var m in mensajes)
                {

                    var output = m.FormatToTelegramMessage();

                    InlineKeyboardMarkup keyboardMarkup = null;
                    
                    if (m.TieneAdjunto == 1)
                    {
                        var kb = InlineKeyboardButton.WithCallbackData($"📎Descargar {m.ArchivoFisico}", $"descargar {m.Id}");
                        keyboardMarkup = new InlineKeyboardMarkup(kb);
                        
                        
                    }
                    
                    Program.botClient.SendTextMessageAsync(chatId, output, ParseMode.Html, replyMarkup: keyboardMarkup).Wait();
                    

                }

                if (mensajes.Count==0)
                {
                    if (!daemonCheck)
                    {
                        var messageId = Program.botClient.EditMessageTextAsync(chatId, messageIdInicio, "No se encontraron nuevos mensajes")
                            .GetAwaiter()
                            .GetResult().MessageId;
                        Thread.Sleep(7000);
                        Program.botClient.DeleteMessageAsync(chatId, messageId);
                    }
                    
                }
                else
                {
                    Program.botClient.DeleteMessageAsync(chatId, messageIdInicio);
                    Program.botClient.SendTextMessageAsync(chatId,
                        "¿Buscar mensajes nuevos?",
                        ParseMode.Default,
                        replyMarkup: new InlineKeyboardMarkup(new[]
                        {
                            new InlineKeyboardButton
                            {
                                Text = "🔄",
                                CallbackData = "retrieve"
                            }
                        }));
                    DBHelper.ActualizarFechaUltimoMensaje(chatId, mensajes.Last().FechaPublicacionCompleta);

                }

                Console.WriteLine($"[SYSTEM] End of Request for {chatId} - Se enviaron: {mensajes.Count} nuevos mensajes");
            }
            catch (Exception e)
            {
                Program.botClient.SendTextMessageAsync(chatId, 
                    "‼Ocurrió un problema mientras procesabamos tu solicitud. \n❌Puede ser porque autogestión no esté disponible en este momento. \n\n" +
                    "Recordá que si cambiaste la contraseña el bot no tiene forma de averiguarlo. Si es así, logueate de nuevo con /login !\n" +
                    "Gracias por usar el bot, y lamentamos el error 🤗", disableNotification: true);
                Console.WriteLine("[UNHANDLED-EXCEPTION] " + e);
            }
        }

        

        public static bool SendBootMessage()
        {
            if (BotSettings.SendBootMessage)
            {
                string mensajeParaEnviar = BotSettings.BootMessage;
                string version = BotSettings.Version;

                List<long> todosLosUsuarios = DBHelper.GetAllChatsID();

                mensajeParaEnviar += $"\n\n<code>botversion:v{version}</code>";
                
                Console.WriteLine($"[SYSTEM] Listo para enviar el mensaje de boot. Procediendo en 1min\nMensaje: {mensajeParaEnviar}");
                Thread.Sleep(30000);
                Console.WriteLine("[SYSTEM] BootMessage: Procediendo en 30s...");
                Thread.Sleep(20000);
                Console.WriteLine("[SYSTEM] BootMessage: Procediendo en 10s...");
                Thread.Sleep(10000);
                Console.WriteLine("[SYSTEM] BootMessage: Procediendo...");
                
                
                
                foreach (var chat in todosLosUsuarios)
                {
                    Program.botClient.SendTextMessageAsync(chat, mensajeParaEnviar, ParseMode.Html);
                    Console.WriteLine($"[SYSTEM] Enviado BootMessage a {chat}");
                    
                }

                Console.WriteLine("[SYSTEM] Mensaje de boot enviado a todos los usuarios");

            }

            return BotSettings.SendBootMessage;
        }

        public static async void LoadAllDaemons()
        {
            Console.WriteLine("[SYSTEM] LOADING DAEMONS...");
            var chatsIds = DBHelper.RetrieveAllAutomaticActives();

            foreach (var chatId in chatsIds)
            {
                var daemon = new AGDaemon(chatId) {DaemonActive = true};
                new Task(() => daemon.Run(true)).Start();
                Console.WriteLine($"[DAEMON] Created for: {chatId}\n[DAEMON] Waiting 10secs...");
                Thread.Sleep(10 * 1000);
            }

            Console.WriteLine("[SYSTEM] ALL DAEMONS LOADED!");
        }
        
    }
}