﻿using System;
using System.Threading;
using BotTelegramAutogestion.Services;

namespace BotTelegramAutogestion.Logic
{
    internal class AGDaemon
    {
        private readonly long chatID;
        private int ultimaConfiguracionDeEspera;
        public AGDaemon(long chatId)
        {
            chatID = chatId;
        }

        public bool DaemonActive { get; set; }

        public void Run(bool silentMode = false)
        {
            try
            {
                var minutesToSleep = DBHelper.GetAutomatedMinutes(chatID);
                this.ultimaConfiguracionDeEspera = minutesToSleep;
                
                var userCred = DBHelper.GetCredentialsForUser(chatID);

                if (!silentMode)
                    Program.EnviarAviso(chatID, "✅ Se configuró de manera automatica la recuperacion de mensajes\n" +
                                                $"⏳ Tiempo entre actualizaciones configurado: {minutesToSleep} minutos");

                while (DaemonActive)
                {
                    Console.WriteLine($"[DAEMON][{userCred.TelegramAlias}:{userCred.UserId}] Running for: {userCred.TelegramAlias} : {userCred.UserId}");

                    var gestorAG = new GestorAutogestion();
                    
                    var mensajes = gestorAG.ObtenerMensajes(userCred.Legajo, userCred.Password, userCred.Especialidad);
                    
                    
                    AsyncFunctions.EnviarMensajesAutogestion(chatID, mensajes, daemonCheck: true);
                    
                    
                    Thread.Sleep(minutesToSleep * 60 * 1000);

                    minutesToSleep = DBHelper.CanAutomatize(chatID); //devuelve 0 si está desactivado
                    
                    if (minutesToSleep != ultimaConfiguracionDeEspera && minutesToSleep!=0)
                    {
                        ultimaConfiguracionDeEspera = minutesToSleep;
                        Program.EnviarAviso(chatID, $"🛠 Se ajustó tu frecuencia de actualizacion a: {ultimaConfiguracionDeEspera} minutos!", true);
                    }
                    if (minutesToSleep <= 0) DaemonActive = false; //saca del ciclo al demonio
                }

                Console.WriteLine($"[DAEMON] Fin del proceso para user: {chatID}");
            }
            catch (Exception e)
            {
                Program.EnviarAviso(969465674, "Ciro ocurrió un error, con una de las cuentas: \n" + e.Message);
                Program.EnviarAviso(286220052, "Lucio ocurrió un error, con una de las cuentas: \n" + e.Message);
                Console.WriteLine("[UNHANDLED-EXCEPTION] " + e);
            }
        }
    }
}