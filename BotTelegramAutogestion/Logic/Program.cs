﻿using System;
using System.Collections.Generic;
using System.Security.Authentication;
using System.Threading;
using System.Threading.Tasks;
using BotTelegramAutogestion.Services;
using Microsoft.Extensions.DependencyInjection;
using Telegram.Bot;
using Telegram.Bot.Args;
using Telegram.Bot.Types.InputFiles;

namespace BotTelegramAutogestion.Logic
{
    internal class Program
    {
        

        public static ITelegramBotClient botClient { get; private set; }

        private static readonly Dictionary<long, DateTime> lastRequestFromUser = new Dictionary<long, DateTime>();

        private static void Main(string[] args)
        {
            Console.Out.WriteLine("[SYSTEM] Starting...");
            try
            {

                var serviceCollection = new ServiceCollection();
                BotSettings.ConfigureServices(serviceCollection);

                botClient = new TelegramBotClient(BotSettings.BotToken);

                var me = botClient.GetMeAsync().Result;

                Console.WriteLine($"[SYSTEM] BOT: id:{me.Id} - Name: {me.Username}");


                bool sent = AsyncFunctions.SendBootMessage();
                
                while (sent)
                {
                    Console.WriteLine("[SYSTEM] BootMensaje Enviado... desactivar la variable de entorno y reiniciar para comenzar a trabajar");
                    Thread.Sleep(3000);
                }
                

                if (BotSettings.LoadDaemons)
                {
                    var loadDeamons = new Task(() => AsyncFunctions.LoadAllDaemons());
                    loadDeamons.Start();
                }
                

                botClient.OnMessage += Event_LlegadaMensaje;
                botClient.OnCallbackQuery += Event_Callback;

                if (BotSettings.Receive)
                {
                    botClient.StartReceiving();
                    Console.WriteLine("[SYSTEM] Receiving...");
                    
                }else Console.Write("[SYSTEM] Not Receiving...");
                
                while (true) Thread.Sleep(30000);
            }
            catch (Exception e)
            {
            }
        }

        private static async void Event_Callback(object sender, CallbackQueryEventArgs eventArgs)
        {
            try
            {
                Console.WriteLine($"[SYSTEM][{eventArgs.CallbackQuery.Message.Chat.Username}:{eventArgs.CallbackQuery.Message.Chat.Id}]CALLBACK:" + eventArgs.CallbackQuery.Data);
                
                var local_chatID = eventArgs.CallbackQuery.Message.Chat.Id;
                if (!lastRequestFromUser.ContainsKey(eventArgs.CallbackQuery.Message.Chat.Id) ||
                    lastRequestFromUser[local_chatID] == null)
                {
                    lastRequestFromUser.Add(local_chatID, DateTime.MinValue);
                }

                var diffEntrePeticiones = DateTime.UtcNow.Subtract(lastRequestFromUser[eventArgs.CallbackQuery.Message.Chat.Id]);

                if (diffEntrePeticiones <= TimeSpan.FromSeconds(5))
                {
                    await botClient.SendTextMessageAsync(eventArgs.CallbackQuery.Message.Chat.Id,
                        "Estas enviando peticiones muy rápido, el server ta chikito 😭\n\nEsperá unos 5s entre cada solicitud",
                        replyToMessageId: eventArgs.CallbackQuery.Message.MessageId);
                    lastRequestFromUser[local_chatID] = DateTime.UtcNow;
                    return;
                }

                lastRequestFromUser[local_chatID] = DateTime.UtcNow;
                
                
                DBHelper.UpdateLastRequestDate(eventArgs.CallbackQuery.Message.Chat.Id);
                
                new Task(()=>ProcessCallback(eventArgs)).Start();
                
            }
            catch (Exception e)

            {
                botClient.SendTextMessageAsync(eventArgs.CallbackQuery.Message.Chat.Id,
                    "‼Ocurrió un problema al procesar tu solicitud. Descripcion error: " + e.Message);
                Console.WriteLine("[UNHANDLED-EXCEPTION] " + e);
            }
        }

        private static void ProcessCallback(CallbackQueryEventArgs eventArgs)
        {
            try
            {
                if (eventArgs.CallbackQuery.Data.Equals("retrieve"))
                {
                    var userDB = DBHelper.GetCredentialsForUser(eventArgs.CallbackQuery.Message.Chat.Id);
                    var mensajes =
                        new GestorAutogestion().ObtenerMensajes(userDB.Legajo, userDB.Password, userDB.Especialidad);
                    AsyncFunctions.EnviarMensajesAutogestion(eventArgs.CallbackQuery.Message.Chat.Id, mensajes);
                    return;
                }

                if (eventArgs.CallbackQuery.Data.StartsWith("descargar "))
                {
                    var result = botClient.SendTextMessageAsync(eventArgs.CallbackQuery.Message.Chat.Id,
                        "⬇⬆📄 Estamos descargando y enviandote el documento... esta tarea puede tardar un poco",
                        replyToMessageId: eventArgs.CallbackQuery.Message.MessageId,
                        disableNotification: true);


                    var user = DBHelper.GetCredentialsForUser(eventArgs.CallbackQuery.Message.Chat.Id);
                    var idDocumentoADescargar = eventArgs.CallbackQuery.Data.Split("descargar ")[1];
                    var stream = GestorAutogestion.DescargarDocumento(user.Legajo, user.Password, user.Especialidad,
                        idDocumentoADescargar);
                    var firIt = eventArgs.CallbackQuery.Message.ReplyMarkup.InlineKeyboard.GetEnumerator();
                    firIt.MoveNext();
                    var secIt = firIt.Current?.GetEnumerator();
                    secIt?.MoveNext();
                    var textDoc = secIt?.Current?.Text.Split("Descargar ")[1];

                    var subdoc = botClient.SendDocumentAsync(eventArgs.CallbackQuery.Message.Chat.Id,
                        new InputOnlineFile(stream, textDoc),
                        replyToMessageId: eventArgs.CallbackQuery.Message.MessageId);
                    Task.WaitAll(result, subdoc);
                    var messageId = result.Result.MessageId;

                    botClient.DeleteMessageAsync(eventArgs.CallbackQuery.Message.Chat.Id, messageId);
                }
            }
            catch (InvalidCredentialException c)
            {
                //Autogestion está Caida
                EnviarAviso(eventArgs.CallbackQuery.Message.Chat.Id,
                    "❌ No pudimos procesar la solicitud.\n\n Posiblemente Autogestion esté caida o bien cambiaste tus credenciaeles.\nVolve a loguearte con /login si cambiaste tu contraseña");
                Console.WriteLine($"[SYSTEM] Excepcion en credenciales: " + c.ToString());
            }
            catch (Exception e)
            {
                Console.WriteLine($"[SYSTEM] Excepcion general en processcallback: " + e.ToString());
            }
        }

        private static async void Event_LlegadaMensaje(object sender, MessageEventArgs eventArgs)
        {
            if (string.IsNullOrEmpty(eventArgs.Message.Text)) return;
            var local_chatID = eventArgs.Message.Chat.Id;
            if (!lastRequestFromUser.ContainsKey(eventArgs.Message.Chat.Id) ||
                lastRequestFromUser[local_chatID] == null)
            {
                lastRequestFromUser.Add(local_chatID, DateTime.MinValue);
            }

            var diffEntrePeticiones = DateTime.UtcNow.Subtract(lastRequestFromUser[eventArgs.Message.Chat.Id]);

            if (diffEntrePeticiones <= TimeSpan.FromSeconds(3))
            {
                await botClient.SendTextMessageAsync(eventArgs.Message.Chat.Id,
                    "Estas enviando peticiones muy rápido, el server ta chikito 😭\n\nEsperá en unos 3s entre cada solicitud",
                    replyToMessageId: eventArgs.Message.MessageId);
                lastRequestFromUser[local_chatID] = DateTime.UtcNow;
                return;
            }

            lastRequestFromUser[local_chatID] = DateTime.UtcNow;
            new Task(() => ProcessNewMessageFromBot(eventArgs)).Start();
        }

        private static void ProcessNewMessageFromBot(MessageEventArgs eventArgs)
        {
            DBHelper.UpdateLastRequestDate(eventArgs.Message.Chat.Id);

            var name = eventArgs.Message.Chat.Username;
            name ??= eventArgs.Message.Chat.Title;
            var mensaje = eventArgs.Message.Text.ToLower();

            if (!mensaje.StartsWith("/login"))
                Console.WriteLine($"[{name}:{eventArgs.Message.Chat.Id}]says: {mensaje}");
            else Console.WriteLine($"[{name}:{eventArgs.Message.Chat.Id}] - Login Requested (password protect)");

            if (mensaje.Equals("/login") || mensaje.Equals("/start"))
            {
                botClient.SendTextMessageAsync(eventArgs.Message.Chat.Id,
                    "Envia /login con tu legajo, contraseña y especialidad separados por espacios \n\npor ejemplo:\n/login 67890 micontraseña sistemas\n\n" +
                    "Tambien podes hacer uso del comando /automatico para recibir de manera automatica los mensajes del autogestion\n\n\n" +
                    "🔐 Quedate tranquilo, tu contraseña está protegida punto a punto por cifrado AES-256, ni siquiera los desarrolladores podemos verla!\n" +
                    "\n❔Cualquier duda que tengas, podes hablar con los devs (descripción del bot) para obtener mas información, somos alumnos de la facu igual que vos",
                    replyToMessageId: eventArgs.Message.MessageId);
                return;
            }


            if (mensaje.StartsWith("/login")) new Task(() => AsyncFunctions.GenerarMensajeLogin(eventArgs)).Start();


            if (mensaje.Equals("/automatico"))
            {
                var chatID = eventArgs.Message.Chat.Id;
                EnviarAvisoTemporal(chatID, "Procesando solicitud...", 6);
                var activeUser = DBHelper.ActiveUser(chatID);
                if (!activeUser)
                {
                    EnviarAviso(chatID,
                        "Su usuario se encuentra inactivo/baneado por el momento no puede usar esta funcion");
                    return;
                }

                var authorizedDaemon = DBHelper.IsAuthorizedAutomaticRetrieve(chatID);
                if (!authorizedDaemon)
                {
                    EnviarAviso(chatID,
                        "⚠Tu usuario no tiene permisos aun para usar la funcion de recuperacion automatica de mensajes.\n\n" +
                        "Por el momento solo algunos usuarios tienen este privilegio, debido a que son los que ayudan a desarrollar y mantener el servidor de hosting de este bot.\n" +
                        "\nEstá planeado que en el futuro esta funcion este disponible para todos\n" +
                        "Contactá a los devs si queres tener acceso o si sos amigo 🤣 asi te damos permiso");
                    return;
                }

                var daemon = new AGDaemon(eventArgs.Message.Chat.Id);
                daemon.DaemonActive = !DBHelper.AutomaticIsActive(eventArgs.Message.Chat.Id); //cambia el estado
                DBHelper.SetAutomaticState(eventArgs.Message.Chat.Id, daemon.DaemonActive);
                if (daemon.DaemonActive) new Task(() => daemon.Run()).Start();
                else
                    EnviarAviso(chatID, "‼ Se desactivó la recuperacion automatica de mensajes ‼");
            }

            Console.WriteLine("[SYSTEM] End of Request "+eventArgs.Message.Chat.Id);
        }

        public static async void EnviarAviso(long chat, string mensaje, bool silentNotification = false)
        {
            botClient.SendTextMessageAsync(chat, mensaje, disableNotification: !silentNotification);
        }

        public static async void EnviarAvisoTemporal(long chat, string mensaje, int segundos)
        {
            var task = botClient.SendTextMessageAsync(chat, mensaje);
            task.Wait();
            var messageId = task.Result.MessageId;
            Thread.Sleep(segundos * 1000);
            botClient.DeleteMessageAsync(chat, messageId);
        }

        
        
        }
}
