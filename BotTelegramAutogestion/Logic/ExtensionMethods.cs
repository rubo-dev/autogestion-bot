﻿using System.Collections.Generic;
using System.Linq;
using BotTelegramAutogestion.Models;

namespace BotTelegramAutogestion.Logic
{
    public static class ExtensionMethods
    {


        public static List<ModelMessage> RemoveOldMessages(this List<ModelMessage> mensajes, long lastDate)
        {

            mensajes.RemoveAll(m => m.FechaPublicacionCompleta <= lastDate);
            
            mensajes = mensajes.OrderBy(o => o.FechaPublicacionCompleta).ToList();

            return mensajes;
        }

        
        
    }
}