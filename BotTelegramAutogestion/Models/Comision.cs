﻿using Newtonsoft.Json;

namespace BotTelegramAutogestion.Models
{
    public class Comision
    {
        [JsonProperty("especialidad")]
        public int Especialidad { get; set; }

        [JsonProperty("plan")]
        public int Plan { get; set; }

        [JsonProperty("materia")]
        public int Materia { get; set; }

        [JsonProperty("anio")]
        public int Anio { get; set; }

        [JsonProperty("numeroComision")]
        public int NumeroComision { get; set; }
    }
}