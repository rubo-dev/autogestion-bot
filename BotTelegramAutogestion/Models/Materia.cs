﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace BotTelegramAutogestion.Models
{
    public class Materia
    {
        [JsonProperty("id")] public string Id { get; set; }
        [JsonProperty("legajo")] public int Legajo { get; set; }
        [JsonProperty("anio")] public int Anio { get; set; }
        [JsonProperty("materia")] public string NombreMateria { get; set; }
        [JsonProperty("curso")] public string Curso { get; set; }
        [JsonProperty("estadoBedelia")] public string EstadoBedelia { get; set; }
        [JsonProperty("estadoAcademica")] public string EstadoAcademica { get; set; }
        [JsonProperty("enlace")] public string Enlace { get; set; }

        public Dictionary<string, string> Notas { get; set; }
    }
}
