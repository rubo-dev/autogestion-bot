﻿using Newtonsoft.Json;

namespace BotTelegramAutogestion.Models
{
    public class ModelCallback
    {
        [JsonProperty("i")] public string IdDocumento { get; set; }

        [JsonProperty("u")] public string User { get; set; }

        [JsonProperty("d")] public string Dominio { get; set; }

        [JsonProperty("p")] public string Pwd { get; set; }
    }
}