﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace BotTelegramAutogestion.Models
{
    public class ModelNota
    {
        [JsonProperty("comision")]
        public Comision Comision { get; set; }

        [JsonProperty("nota1")]
        public string Nota1 { get; set; }

        [JsonProperty("nota2")]
        public string Nota2 { get; set; }

        [JsonProperty("nota3")]
        public string Nota3 { get; set; }

        [JsonProperty("nota4")]
        public string Nota4 { get; set; }

        [JsonProperty("nota5")]
        public string Nota5 { get; set; }

        [JsonProperty("nota6")]
        public string Nota6 { get; set; }

        [JsonProperty("nota7")]
        public string Nota7 { get; set; }

        [JsonProperty("nota8")]
        public string Nota8 { get; set; }

        [JsonProperty("nota9")]
        public string Nota9 { get; set; }

        [JsonProperty("nota10")]
        public string Nota10 { get; set; }

        public List<string> Notas
        {
            get
            {
                var notas = new List<string>
                {
                    Nota1,
                    Nota2,
                    Nota3,
                    Nota4,
                    Nota5,
                    Nota6,
                    Nota7,
                    Nota8,
                    Nota9,
                    Nota10
                };

                return notas;
            }
        }
    }
}
